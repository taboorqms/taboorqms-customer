//package com.taboor.qms.customer.auth;
//
//import java.io.IOException;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import com.taboor.qms.core.auth.JwtTokenProvider;
//import com.taboor.qms.core.auth.MyUserDetails;
//import com.taboor.qms.core.auth.ValidateTokenResponse;
//import com.taboor.qms.core.model.Session;
//import com.taboor.qms.core.utils.TaboorQMSCoreUtils;
//
//@Component
//public class JwtAuthenticationFilter extends OncePerRequestFilter {
//
//	@Autowired
//	private JwtTokenProvider tokenProvider;
//
//	@Autowired
//	private MyUserDetailsService userDetailsService;
//
//	private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
//
//	@Override
//	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
//			throws ServletException, IOException {
//		try {
//			String jwt = TaboorQMSCoreUtils.getJwtFromRequest(request);
//			logger.info("Method: " + request.getMethod() + " " + request.getRequestURL().toString());
//
//			if (StringUtils.hasText(jwt) && !request.getMethod().equals(HttpMethod.OPTIONS.toString())) {
//				ValidateTokenResponse validateTokenResponse = tokenProvider.validateToken(jwt);
//
//				if (validateTokenResponse.getValue()) {
//					Session session = validateTokenResponse.getSession();
//					UserDetails userDetails = null;
//
//					if (session.getUser() == null)
//						userDetails = new MyUserDetails(session, null);
//					else
//						userDetails = userDetailsService.loadUserBySession(session);
//
//					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
//							userDetails, null, userDetails.getAuthorities());
//					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//
//					SecurityContextHolder.getContext().setAuthentication(authentication);
//				}
//			}
//		} catch (Exception ex) {
//			logger.error("Could not set user authentication in security context", ex);
//		}
//		filterChain.doFilter(request, response);
//	}
//
//}