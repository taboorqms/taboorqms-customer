//package com.taboor.qms.customer.auth;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.taboor.qms.core.auth.MyUserDetails;
//import com.taboor.qms.core.exception.TaboorQMSServiceException;
//import com.taboor.qms.core.exception.xyzErrorMessage;
//import com.taboor.qms.core.model.Session;
//import com.taboor.qms.core.utils.GenericMapper;
//import com.taboor.qms.core.utils.PrivilegeName;
//
//@Service
//public class MyUserDetailsService implements UserDetailsService {
//	private RestTemplate restTemplate = new RestTemplate();
//
//	@Value("${url.microservice.db.connector}")
//	private String dbConnectorMicroserviceURL;
//
//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		return null;
//	}
//
//	public UserDetails loadUserBySession(Session session) throws Exception {
//
//		String uri = dbConnectorMicroserviceURL + "/tab/userprivilegemapper/getPrivilegeName/user?userId="
//				+ session.getUser().getUserId();
//		ResponseEntity<?> getPrivilegeNamesResponse = restTemplate.getForEntity(uri, List.class);
//		if (!getPrivilegeNamesResponse.getStatusCode().equals(HttpStatus.OK))
//			throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
//					+ xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
//		List<PrivilegeName> privilegeNames = GenericMapper.convertListToObject(getPrivilegeNamesResponse.getBody(),
//				new TypeReference<List<PrivilegeName>>() {
//				});
//
//		List<GrantedAuthority> authorities = privilegeNames.stream()
//				.map(entity -> new SimpleGrantedAuthority("ROLE_" + entity.name())).collect(Collectors.toList());
//
//		return new MyUserDetails(session, authorities);
//	}
//}