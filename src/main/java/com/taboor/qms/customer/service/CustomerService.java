package com.taboor.qms.customer.service;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.core.exception.TaboorQMSDaoException;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.payload.ChangeProfilePayload;
import com.taboor.qms.core.payload.RegisterCustomerPayload;
import com.taboor.qms.core.payload.UpdateCustomerSettingsPayload;
import com.taboor.qms.core.response.BooleanResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetCustomerSettingsResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface CustomerService {

	public GenStatusResponse registerCustomer(@Valid RegisterCustomerPayload registerCustomerPayload,
			MultipartFile profileImage) throws TaboorQMSServiceException, IOException, TaboorQMSDaoException, Exception;

	public GenStatusResponse changeCustomerInfo(@Valid ChangeProfilePayload changeProfilePayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse deleteCustomer() throws TaboorQMSServiceException, Exception;

	public GetValuesResposne getCustomerBookmarkedBranchIds() throws TaboorQMSServiceException, Exception;

	public GenStatusResponse updateBranchBookmark(Long branchId) throws TaboorQMSServiceException, Exception;

	public GenStatusResponse setCustomerSettings(UpdateCustomerSettingsPayload payload)
			throws TaboorQMSServiceException, Exception;

	public GetCustomerSettingsResponse getCustomerSettings() throws TaboorQMSServiceException, Exception;

	public GetBranchListResponse getCustomerBookmarkedBranches() throws TaboorQMSServiceException, Exception;

	public GetValuesResposne countCustomerBookmarkedBranches() throws TaboorQMSServiceException, Exception;

	public BooleanResponse checkBranchBookmark(long branchId) throws TaboorQMSServiceException, Exception;
	
//	public GetCustomerPicResponse getCustomerPicture()
//			throws TaboorQMSServiceException, IOException, TaboorQMSDaoException, Exception;
	
	public GenStatusResponse updateCustomerPicture(MultipartFile profileImage)
			throws TaboorQMSServiceException, IOException, TaboorQMSDaoException, Exception;
}
