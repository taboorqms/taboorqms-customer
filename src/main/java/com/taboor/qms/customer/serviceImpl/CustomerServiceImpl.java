package com.taboor.qms.customer.serviceImpl;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSDaoException;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchBookmark;
import com.taboor.qms.core.model.Customer;
import com.taboor.qms.core.model.CustomerSettings;
import com.taboor.qms.core.model.PaymentMethod;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.payload.ChangeProfilePayload;
import com.taboor.qms.core.payload.RegisterCustomerPayload;
import com.taboor.qms.core.payload.RegisterUserPayload;
import com.taboor.qms.core.payload.UpdateCustomerSettingsPayload;
import com.taboor.qms.core.response.BooleanResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetCustomerSettingsResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.response.RegisterUserResponse;
import com.taboor.qms.core.utils.FileDataObject;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.core.utils.TaboorQMSConfigurations;
import com.taboor.qms.customer.service.CustomerService;

import net.bytebuddy.utility.RandomString;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	RestTemplate restTemplate = new RestTemplate();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Value("${url.microservice.user.management}")
	private String userManagementMicroserviceURL;

	@Value("${taboor.credit.payment.method.id}")
	private long taboorCreditPaymentMethodId;

	@Autowired
	private FileDataService fileDataService;

	@Override
	public GenStatusResponse registerCustomer(@Valid RegisterCustomerPayload registerCustomerPayload,
			MultipartFile profileImage) throws TaboorQMSServiceException, TaboorQMSDaoException, Exception {

		UserRole getUserRoleResponse = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/userroles/get/roleName?roleName=" + RoleName.Customer,
				UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());

		RegisterUserPayload registerUserPayload = new RegisterUserPayload();
		registerUserPayload.setName(registerCustomerPayload.getName());
		registerUserPayload.setEmail(registerCustomerPayload.getEmail());
		registerUserPayload.setPassword(registerCustomerPayload.getPassword());
		registerUserPayload.setPhoneNumber(registerCustomerPayload.getPhoneNumber());
		registerUserPayload.setUserRole(getUserRoleResponse);
		registerUserPayload.setAssignRolePrivileges(true);

		MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
		bodyMap.add("RegisterUserPayload", registerUserPayload);
		if (profileImage != null)
			bodyMap.add("UserProfileImage", profileImage.getResource());

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap,
				HttpHeadersUtils.getMultipartFormDataHeader());
		System.out.println("Here"+requestEntity);
		RegisterUserResponse registerUserResponse = RestUtil.postRequest(
				userManagementMicroserviceURL + "/user/register", requestEntity, RegisterUserResponse.class,
				xyzErrorMessage.USER_NOT_REGISTERED.getErrorCode(),
				xyzErrorMessage.USER_NOT_REGISTERED.getErrorDetails());
		System.out.println("Here2");

//		String image = profileImage.getOriginalFilename();
//		logger.info("File Type: " + image.substring(image.indexOf(".") + 1));
//		String imageName = "IMG-" + Base64.getEncoder().encodeToString(registerCustomerPayload.getEmail().getBytes())
//				+ "." + image.substring(image.indexOf(".") + 1);
		Customer customer = new Customer();
		customer.setUser(registerUserResponse.getUser());
//		customer.setPictureUrl(EmailTemplates.PICTURE_SAVE_LINK + imageName);
		System.out.println("Here3");

		ResponseEntity<Customer> createCustomerResponse = saveCustomer(customer);

		if (!createCustomerResponse.getStatusCode().equals(HttpStatus.OK) || createCustomerResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.CUSTOMER_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.CUSTOMER_NOT_CREATED.getErrorDetails());

		customer = createCustomerResponse.getBody();
		System.out.println("Here4");
   
		// saving picture
//		saveCustomerPicture(profileImage, imageName);

		CustomerSettings customerSettings = new CustomerSettings();
		customerSettings.setCustomer(customer);
		customerSettings.setNotificationEmail(false);
		customerSettings.setNotificationPushNotification(true);
		customerSettings.setNotificationSMS(true);
		customerSettings.setPrivacyEmail(true);
		customerSettings.setPrivacyName(true);
		customerSettings.setPrivacyPhoneNumber(false);
		customerSettings.setShowBranchesDistance(true);
		customerSettings.setShowBranchesFastestQueue(false);

		HttpEntity<CustomerSettings> customerSettingsEntity = new HttpEntity<>(customerSettings,
				HttpHeadersUtils.getApplicationJsonHeader());
		RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/customersettings/save", customerSettingsEntity,
				CustomerSettings.class, xyzErrorMessage.CUSTOMERSETTING_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.CUSTOMERSETTING_NOT_CREATED.getErrorDetails());

		PaymentMethod getPaymentMethodResponse = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/paymentmethods/get/id?paymentMethodId="
						+ taboorCreditPaymentMethodId,
				PaymentMethod.class, xyzErrorMessage.TABOOR_PAYMENT_METHOD_INVALID.getErrorCode(),
				xyzErrorMessage.TABOOR_PAYMENT_METHOD_INVALID.getErrorDetails());

		UserBankCard userBankCard = new UserBankCard();
		userBankCard.setCardTitle(customer.getUser().getName());
		userBankCard.setCardNumber("Taboor-" + customer.getUser().getUserId());
		userBankCard.setCardType("Taboor Credit");
		userBankCard.setExpiryMonth(0);
		userBankCard.setExpiryYear(0);
		userBankCard.setCardCvv(null);
		userBankCard.setPaymentMethod(getPaymentMethodResponse);
		userBankCard.setBalance(0.0);
		userBankCard.setUser(customer.getUser());

		HttpEntity<UserBankCard> entity = new HttpEntity<>(userBankCard, HttpHeadersUtils.getApplicationJsonHeader());
		RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/userbankcards/save", entity, UserBankCard.class,
				xyzErrorMessage.USERBANKCARD_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.USERBANKCARD_NOT_CREATED.getErrorDetails());

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.CUSTOMER_REGISTERED.getMessage());

	}

	private Customer findByUserEmail(String email) throws TaboorQMSServiceException {
		String uri = dbConnectorMicroserviceURL + "/tab/customers/get/userEmail?email=" + email;
		ResponseEntity<Customer> getByUserEmailResponse = restTemplate.getForEntity(uri, Customer.class);
		return getByUserEmailResponse.getBody();
	}

	private Customer findByUserId(Long userId) throws TaboorQMSServiceException {

		Customer getByUserEmailResponse = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/customers/get/userId?userId=" + userId, Customer.class,
				xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorDetails());

		return getByUserEmailResponse;
	}

	private ResponseEntity<Customer> saveCustomer(Customer customer) throws TaboorQMSServiceException {
		String uri = dbConnectorMicroserviceURL + "/tab/customers/save";
		HttpEntity<Customer> customerEntity = new HttpEntity<>(customer, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<Customer> createCustomerResponse = restTemplate.postForEntity(uri, customerEntity,
				Customer.class);
		return createCustomerResponse;
	}

	@Override
	public GenStatusResponse changeCustomerInfo(@Valid ChangeProfilePayload changeProfilePayload)
			throws TaboorQMSServiceException, Exception {

		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		Customer customer = findByUserId(session.getUser().getUserId());

		if (changeProfilePayload.getName() != null) {
			customer.getUser().setName(changeProfilePayload.getName());
		}

		ResponseEntity<Customer> createCustomerResponse = saveCustomer(customer);

		if (!createCustomerResponse.getStatusCode().equals(HttpStatus.OK) || createCustomerResponse.getBody() == null)
			throw new TaboorQMSServiceException(444 + "::" + "Customer Not Updated Successfully");

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.CUSTOMER_UPDATED.getMessage());
	}

	@Override
	public GenStatusResponse deleteCustomer() throws TaboorQMSServiceException, Exception {

		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		String uri = userManagementMicroserviceURL + "/user/delete";

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setBearerAuth(session.getSessionToken());

		ResponseEntity<GenStatusResponse> deleteUserResponse = restTemplate.exchange(uri, HttpMethod.GET,
				new HttpEntity<>(httpHeaders), GenStatusResponse.class);

		return deleteUserResponse.getBody();
	}

	@Override
	public GetValuesResposne getCustomerBookmarkedBranchIds() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		Customer customer = findByUserId(session.getUser().getUserId());
		String uri = dbConnectorMicroserviceURL + "/tab/branchbookmarks/getBranchIds/userId?userId="
				+ customer.getUser().getUserId();
		ResponseEntity<?> getCustomerBookmarkedBranchIdsResponse = restTemplate.getForEntity(uri, List.class);

		if (getCustomerBookmarkedBranchIdsResponse.getStatusCode().equals(HttpStatus.OK))
			throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
					+ xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

		List<Integer> branchIdsList = GenericMapper.convertListToObject(
				getCustomerBookmarkedBranchIdsResponse.getBody(), new TypeReference<List<Integer>>() {
				});

		Map<String, Object> values = new HashMap<String, Object>();
		if (branchIdsList != null)
			for (int i = 0; i < branchIdsList.size(); i++)
				values.put("BranchId-" + i, String.valueOf(branchIdsList.get(i)));
		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BOOKMARKED_BRANCH_FETCHED.getMessage(), values);
	}

	@Override
	public GetValuesResposne countCustomerBookmarkedBranches() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		Customer customer = findByUserId(session.getUser().getUserId());
		String uri = dbConnectorMicroserviceURL + "/tab/branchbookmarks/getCount/userId?userId="
				+ customer.getUser().getUserId();
		ResponseEntity<Integer> countCustomerBookmarkedBranchesResponse = restTemplate.getForEntity(uri, Integer.class);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("BookmarkedBranchesCount", String.valueOf(countCustomerBookmarkedBranchesResponse.getBody()));
		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BOOKMARKED_BRANCH_COUNTED.getMessage(), values);
	}

	@Override
	public GetBranchListResponse getCustomerBookmarkedBranches() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		Customer customer = findByUserId(session.getUser().getUserId());
		String uri = dbConnectorMicroserviceURL + "/tab/branchbookmarks/get/user?userId="
				+ customer.getUser().getUserId();
		ResponseEntity<?> getCustomerBookmarkedBranchesResponse = restTemplate.getForEntity(uri, List.class);

		List<Branch> branchList = GenericMapper.convertListToObject(getCustomerBookmarkedBranchesResponse.getBody(),
				new TypeReference<List<Branch>>() {
				});

		return new GetBranchListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BOOKMARKED_BRANCH_FETCHED.getMessage(), branchList);
	}

	@Override
	public BooleanResponse checkBranchBookmark(long branchId) throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		Customer customer = findByUserId(session.getUser().getUserId());

		Boolean checkCustomerBookmarkedBranchResponse = RestUtil
				.get(dbConnectorMicroserviceURL + "/tab/branchbookmarks/check/userAndBranch?userId="
						+ customer.getUser().getUserId() + "&branchId=" + branchId, Boolean.class);

		return new BooleanResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BOOKMARKED_BRANCH_CHECKED.getMessage(), checkCustomerBookmarkedBranchResponse);
	}

	@Override
	public GenStatusResponse updateBranchBookmark(Long branchId) throws TaboorQMSServiceException, Exception {

		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		Customer customer = findByUserId(session.getUser().getUserId());

		String uri = dbConnectorMicroserviceURL + "/tab/branchbookmarks/getId/userAndBranch?userId="
				+ customer.getUser().getUserId() + "&branchId=" + branchId;
		ResponseEntity<Long> getIdByCustomerAndBranchResponse = restTemplate.getForEntity(uri, Long.class);

		if (getIdByCustomerAndBranchResponse.getStatusCode().equals(HttpStatus.OK)
				&& getIdByCustomerAndBranchResponse.getBody() == null) {

			Branch getBranchResponse = RestUtil.getRequest(
					dbConnectorMicroserviceURL + "/tab/branches/get/id?branchId=" + branchId, Branch.class,
					xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode(),
					xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());

			BranchBookmark branchBookmark = new BranchBookmark();
			branchBookmark.setUser(customer.getUser());
			branchBookmark.setBranch(getBranchResponse);

			uri = dbConnectorMicroserviceURL + "/tab/branchbookmarks/save";
			HttpEntity<BranchBookmark> branchBookmarkEntity = new HttpEntity<>(branchBookmark,
					HttpHeadersUtils.getApplicationJsonHeader());
			ResponseEntity<BranchBookmark> createBranchBookmarkResponse = restTemplate.postForEntity(uri,
					branchBookmarkEntity, BranchBookmark.class);
			if (!createBranchBookmarkResponse.getStatusCode().equals(HttpStatus.OK)
					|| createBranchBookmarkResponse.getBody() == null)
				throw new TaboorQMSServiceException(xyzErrorMessage.BRANCHBOOKMARK_NOT_CREATED.getErrorCode() + "::"
						+ xyzErrorMessage.BRANCHBOOKMARK_NOT_CREATED.getErrorDetails());
		} else {
			uri = dbConnectorMicroserviceURL + "/tab/branchbookmarks/delete?branchBookmarkId="
					+ getIdByCustomerAndBranchResponse.getBody();
			ResponseEntity<Integer> deleteBranchBookmarkResponse = restTemplate.getForEntity(uri, Integer.class);
			if (!deleteBranchBookmarkResponse.getStatusCode().equals(HttpStatus.OK)
					|| deleteBranchBookmarkResponse.getBody().equals(0))
				throw new TaboorQMSServiceException(xyzErrorMessage.BRANCHBOOKMARK_NOT_UPDATED.getErrorCode() + "::"
						+ xyzErrorMessage.BRANCHBOOKMARK_NOT_UPDATED.getErrorDetails());
		}

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BOOKMARKED_BRANCH_UPADTED.getMessage());
	}

	@Override
	public GenStatusResponse setCustomerSettings(UpdateCustomerSettingsPayload payload)
			throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		Customer customer = findByUserId(session.getUser().getUserId());

		CustomerSettings customerSettings = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/customersettings/getByCustomerId?customerId="
						+ customer.getCustomerId(),
				CustomerSettings.class, xyzErrorMessage.CUSTOMERSETTING_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.CUSTOMERSETTING_NOT_EXISTS.getErrorDetails());

		customerSettings.setNotificationEmail(payload.getNotificationEmail());
		customerSettings.setNotificationPushNotification(payload.getNotificationPushNotification());
		customerSettings.setNotificationSMS(payload.getNotificationSMS());
		customerSettings.setPrivacyEmail(payload.getPrivacyEmail());
		customerSettings.setPrivacyName(payload.getPrivacyName());
		customerSettings.setPrivacyPhoneNumber(payload.getPrivacyPhoneNumber());
		customerSettings.setShowBranchesDistance(payload.getShowBranchesDistance());
		customerSettings.setShowBranchesFastestQueue(payload.getShowBranchesFastestQueue());

		HttpEntity<CustomerSettings> customerSettingsEntity = new HttpEntity<>(customerSettings,
				HttpHeadersUtils.getApplicationJsonHeader());
		RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/customersettings/save", customerSettingsEntity,
				CustomerSettings.class, xyzErrorMessage.CUSTOMERSETTING_NOT_UPDATED.getErrorCode(),
				xyzErrorMessage.CUSTOMERSETTING_NOT_UPDATED.getErrorDetails());

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.CUSTOMER_SETTING_UPDATED.getMessage());

	}

	@Override
	public GetCustomerSettingsResponse getCustomerSettings() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		Customer customer = findByUserId(session.getUser().getUserId());

		CustomerSettings getCustomerSettings = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/customersettings/getByCustomerId?customerId="
						+ customer.getCustomerId(),
				CustomerSettings.class, xyzErrorMessage.CUSTOMERSETTING_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.CUSTOMERSETTING_NOT_EXISTS.getErrorDetails());

		getCustomerSettings.setCustomer(null);
		return new GetCustomerSettingsResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.CUSTOMER_SETTING_FETCHED.getMessage(), getCustomerSettings);
	}

	@Override
	public GenStatusResponse updateCustomerPicture(MultipartFile profileImage)
			throws TaboorQMSServiceException, IOException, TaboorQMSDaoException, Exception {
		System.out.println("here");
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		System.out.println(myUserDetails);
		User user = myUserDetails.getSession().getUser();
		System.out.println(user);
		String previousURL = user.getProfileImageUrl();
		Boolean removed = true;
		if(previousURL.length() > 0) {
			fileDataService
						.remove(Arrays.asList(new String(Base64.getDecoder().decode(user.getProfileImageUrl().getBytes()))));
		}
		
		
		if (removed) {
			if (!Arrays.asList("image/jpeg", "image/jpg", "image/png").contains(profileImage.getContentType()))
				throw new TaboorQMSServiceException(xyzErrorMessage.IMAGE_FORMAT_WRONG.getErrorCode() + "::"
						+ xyzErrorMessage.IMAGE_FORMAT_WRONG.getErrorDetails());

			String filePath = TaboorQMSConfigurations.HOST_USER_IMAGE_FOLDER.getValue()
					.concat(String.valueOf(OffsetDateTime.now().toEpochSecond()).concat(RandomString.make(10)))
					.concat(profileImage.getOriginalFilename()
							.substring(profileImage.getOriginalFilename().indexOf(".")));
			Boolean saved = fileDataService.save(Arrays.asList(new FileDataObject(profileImage, filePath)));
			System.out.println("here");
			if (saved) {
				user.setProfileImageUrl(Base64.getEncoder().encodeToString(filePath.getBytes()));
				RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/users/save",
						new HttpEntity<>(user, HttpHeadersUtils.getApplicationJsonHeader()), User.class,
						xyzErrorMessage.USER_NOT_UPDATED.getErrorCode(),
						xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
				return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
						xyzResponseMessage.CUSTOMER_PICTURE_UPDATED.getMessage());
			} else
				return new GenStatusResponse(xyzResponseCode.ERROR.getCode(),
						xyzErrorMessage.CUSTOMER_NOT_UPDATED.getErrorDetails());
		}
		return new GenStatusResponse(xyzResponseCode.ERROR.getCode(),
				xyzErrorMessage.CUSTOMER_NOT_UPDATED.getErrorDetails());

	}

}
