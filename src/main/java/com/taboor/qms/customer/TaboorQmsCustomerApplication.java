package com.taboor.qms.customer;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.taboor.qms.customer", "com.taboor.qms.core" })
public class TaboorQmsCustomerApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(TaboorQmsCustomerApplication.class, args);
	}
        
        @Override
    public void run(String... args) throws Exception {

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(getClass().getClassLoader()
                        .getResourceAsStream("taboor-firebase-adminsdk-0p6y8-b25b763338.json")))
                .setDatabaseUrl("https://taboor.firebaseio.com").build();

        FirebaseApp.initializeApp(options);
    }

}
