package com.taboor.qms.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.taboor.qms.core.auth.JwtAuthenticationFilter;
import com.taboor.qms.core.utils.PrivilegeName;

@EnableWebSecurity
class AuthWebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService myUserDetailsService;

	@Autowired
	private JwtAuthenticationFilter jwtAuthenticationFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(myUserDetailsService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeRequests()
				.antMatchers("/h2-console/**", "/swagger-ui.html", "/webjars/**", "/swagger-resources/**", "/v2/**")
				.permitAll().antMatchers("/settings/get").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/settings/update").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/change").hasAnyRole(PrivilegeName.Customer_Actions.name()).antMatchers("/delete")
				.hasAnyRole(PrivilegeName.Customer_Actions.name()).antMatchers("/branchbookmark/getBranchIds/user")
				.hasAnyRole(PrivilegeName.Customer_Actions.name()).antMatchers("/branchbookmark/getCount/user")
				.hasAnyRole(PrivilegeName.Customer_Actions.name()).antMatchers("/branchbookmark/get/user")
				.hasAnyRole(PrivilegeName.Customer_Actions.name()).antMatchers("/branchbookmark/update")
				.hasAnyRole(PrivilegeName.Customer_Actions.name()).antMatchers("/branchbookmark/check")
				.hasAnyRole(PrivilegeName.Customer_Actions.name()).antMatchers("/get/picture")
				.hasAnyRole(PrivilegeName.Customer_Actions.name()).antMatchers("/update/picture")
				.hasAnyRole(PrivilegeName.Customer_Actions.name())
				// ByPass Routes
				.antMatchers("/register").permitAll().anyRequest().denyAll();
		httpSecurity.headers().frameOptions().disable();
		httpSecurity.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

	}

}