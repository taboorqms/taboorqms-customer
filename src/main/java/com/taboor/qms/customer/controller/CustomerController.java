package com.taboor.qms.customer.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.core.payload.ChangeProfilePayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.payload.RegisterCustomerPayload;
import com.taboor.qms.core.payload.UpdateCustomerSettingsPayload;
import com.taboor.qms.core.response.BooleanResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetCustomerSettingsResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.response.LoginUserResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.customer.service.CustomerService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping
@CrossOrigin(origins = "*")
public class CustomerController {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	CustomerService customerService;

	@ApiOperation(value = "Register Customer in the db.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully created customer"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = {
			"multipart/form-data" }, produces = "application/json")
	public @ResponseBody GenStatusResponse registerCustomer(
			@RequestParam(value = "RegisterCustomerPayload", required = true) @Valid final String registerPayload,
			@RequestParam(value = "CustomerProfileImage", required = false) @Valid final MultipartFile profileImage) throws Exception {
		logger.info("Calling Register Customer - API");
		RegisterCustomerPayload registerCustomerPayload = GenericMapper.jsonToObjectMapper(registerPayload,
				RegisterCustomerPayload.class);
		return customerService.registerCustomer(registerCustomerPayload, profileImage);
	}

	@ApiOperation(value = "Edit Customer Info in the system.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer Information Updated Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/change", method = RequestMethod.POST)
	public @ResponseBody GenStatusResponse changeCustomer(
			@RequestBody @Valid final ChangeProfilePayload changeProfilePayload) throws Exception, Throwable {
		logger.info("Calling Login Customer - API");
		return customerService.changeCustomerInfo(changeProfilePayload);
	}

	@ApiOperation(value = "Delete Customer from the system.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer deleted Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GenStatusResponse deleteCustomer() throws Exception, Throwable {
		logger.info("Calling Delete Customer - API");
		return customerService.deleteCustomer();
	}

	@ApiOperation(value = "Get Customer All Bookmarked Branch ID's.", response = GetValuesResposne.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Customer All Bookmarked Branch ID's fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/branchbookmark/getBranchIds/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetValuesResposne getCustomerBookmarkedBranchIds() throws Exception, Throwable {
		logger.info("Calling Get Customer All Bookmarked Branch ID's - API");
		return customerService.getCustomerBookmarkedBranchIds();
	}

	@ApiOperation(value = "Get Customer Bookmarked Branches Count.", response = GetValuesResposne.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer Bookmarked Branches counted Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/branchbookmark/getCount/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetValuesResposne countCustomerBookmarkedBranches() throws Exception, Throwable {
		logger.info("Calling Get Customer Bookmarked Branches Count - API");
		return customerService.countCustomerBookmarkedBranches();
	}

	@ApiOperation(value = "Get Customer All Bookmarked Branches.", response = GetBranchListResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer All Bookmarked Branches fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/branchbookmark/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetBranchListResponse getCustomerBookmarkedBranches() throws Exception, Throwable {
		logger.info("Calling Get Customer All Bookmarked Branches - API");
		return customerService.getCustomerBookmarkedBranches();
	}

	@ApiOperation(value = "Update Branch Bookmark.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Branch Bookmark updated Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/branchbookmark/update", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse updateCustomerBookmarkedBranch(@RequestBody GetByIdPayload branchId)
			throws Exception, Throwable {
		logger.info("Calling Update Branch Bookmark - API");
		return customerService.updateBranchBookmark(branchId.getId());
	}

	@ApiOperation(value = "Check Branch Bookmark.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Branch Bookmark checked Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/branchbookmark/check", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody BooleanResponse checkCustomerBookmarkedBranch(@RequestBody GetByIdPayload branchId)
			throws Exception, Throwable {
		logger.info("Calling Check Branch Bookmark - API");
		return customerService.checkBranchBookmark(branchId.getId());
	}

	@ApiOperation(value = "Update Customer Settings in the system.", response = LoginUserResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer Settings Updated Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/settings/update", method = RequestMethod.POST)
	public @ResponseBody GenStatusResponse updateCustomerSettings(
			@RequestBody @Valid final UpdateCustomerSettingsPayload payload) throws Exception, Throwable {
		logger.info("Calling Update Customer Settings - API");
		return customerService.setCustomerSettings(payload);
	}

	@ApiOperation(value = "Get Customer Settings from the system.", response = LoginUserResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get Customer Settings Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/settings/get", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetCustomerSettingsResponse getCustomerSettings() throws Exception, Throwable {
		logger.info("Calling Get Customer Settings - API");
		return customerService.getCustomerSettings();
	}

//	@RequestMapping(value = "/get/picture", method = RequestMethod.GET, produces = "application/json")
//	public GetCustomerPicResponse  getCustomerPicture() throws Exception {
//		logger.info("Calling Get Customer Picture - API");
//		return customerService.getCustomerPicture();
//	}

	@RequestMapping(value = "/update/picture", method = RequestMethod.POST, produces = "application/json")
	public GenStatusResponse updateCustomerPicture(
			@RequestParam(value = "CustomerProfileImage") @Valid final MultipartFile profileImage) throws Exception {
		logger.info("Calling Update Customer Picture - API");
		return customerService.updateCustomerPicture(profileImage);
	}
}
